#!/usr/bin/env python3

import sys
from pathlib import Path
import os
import stat
from subprocess import check_output
from enum import Enum


class Member(Enum):
    CTOR = 0
    CPCTOR = 1
    MVCTOR = 2
    CPASST = 3
    MVASST = 4
    DTOR = 5


className = "Test"


MemberSignature = {
    Member.CTOR: f"{className}::{className}()",
    Member.CPCTOR: f"{className}::{className}({className} const&)",
    Member.MVCTOR: f"{className}::{className}({className}&&)",
    Member.CPASST: f"{className}::operator=({className} const&)",
    Member.MVASST: f"{className}::operator=({className}&&)",
    Member.DTOR: f"{className}::~{className}()",
}


def has_member(path, member):
    symbols = list_symbols(path)
    return MemberSignature[member] in symbols


def list_binaries(dir):
    binaries_str = check_output(
        ["find", dir.absolute(), "-maxdepth", "1", "-type", "f", "-executable"],
        text=True,
    ).splitlines()
    return map(Path, map(str, filter(bool, binaries_str)))


def process_name(path):
    return map(lambda x: x.split("-"), path.name.split("-IN-LIB-"))


def list_symbols(path):
    nm_str = check_output(["llvm-nm", "-gjCU", path.absolute()], text=True).splitlines()
    return map(str, nm_str)


def transform_to_dict(combos, binaries):
    result = dict()
    bins = list(binaries)
    for idx, combo in enumerate(combos):
        lcombo = list(combo)
        generated, precondition = lcombo
        print('Index', idx, generated, precondition)
        if has_member(bins[idx], Member[generated[0]]):
            if tuple(precondition) not in result:
                result[tuple(precondition)] = generated
            else:
                result[tuple(precondition)].extend(generated)
    return result


def compress(members):
    n = 0
    for member in members:
        if member:
            n = n | (1 << Member[member].value)
    return n


def export_map(data, filename):
    import json

    with open(filename, "w") as fl:
        fl.write(json.dumps(data, indent=4, sort_keys=True))


def main():
    if len(sys.argv) < 2:
        print("Need build path as argument")
        exit(-1)
    buildpath = Path(sys.argv[1])
    combos = map(process_name, list_binaries(buildpath))
    combomap = transform_to_dict(combos, list_binaries(buildpath))

    import pprint

    compressedmap = dict()
    for key, val in combomap.items():
        compressedmap[compress(key)] = compress(val)

    print(f"compressed map has {len(compressedmap)} keys")
    export_map(compressedmap, "mapping.json")


if __name__ == "__main__":
    main()
