#include <iostream>
#include <type_traits>
#include "test.h"

#define USE_FN(fn) auto *_ptr_fn = &fn

// https://stackoverflow.com/a/51912859
template<typename T>
struct Holder
{
    operator T const &();
    operator T&&();
};

template <typename T>
constexpr auto is_truly_move_constructible() {
    return std::is_move_constructible_v<T> && !std::is_constructible_v<T, Holder<T>>;
}

#if defined(TEST_CTOR)
static_assert(std::is_default_constructible_v<Test>);

void test_ctor()
{
    std::cout << "TEST_CTOR" << std::endl;
    Test t;
}

USE_FN(test_ctor);

#endif

#if defined(TEST_CPCTOR)
static_assert(std::is_copy_constructible_v<Test>);

auto test_cpctor(Test &a)
{
    std::cout << "TEST_CPCTOR" << std::endl;
    return Test{a};
}
#endif

#if defined(TEST_MVCTOR)
static_assert(is_truly_move_constructible<Test>());

auto test_mvctor(Test &a)
{
    std::cout << "TEST_MVCTOR" << std::endl;
    return Test{std::move(a)};
}
#endif

#if defined(TEST_CPASST)
static_assert(std::is_copy_assignable_v<Test>);

void test_cpasst(Test &a, Test &b)
{
    std::cout << "TEST_CPASST" << std::endl;
    b = a;
}
#endif

#if defined(TEST_MVASST)
void test_mvasst(Test &a, Test &b) {
    a = std::move(b);
}
#endif

#if defined(TEST_DTOR)
static_assert(std::is_destructible_v<Test>);

void test_dtor(Test &test)
{
    std::cout << "TEST_DTOR" << std::endl;
    test.~Test();
}
#endif

int main()
{
    return 0;
}