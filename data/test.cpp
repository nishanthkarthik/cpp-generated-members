#include <iostream>
#include <utility>
#include <memory>

#include "test.h"

#if defined(CTOR)
Test::Test()
{
    t = "test";
    print();
    std::cout << "ctor" << std::endl;
}
#endif

#if defined(CPCTOR)
Test::Test(const Test &other) : t{other.t}
{
    other.print();
    print();
    std::cout << "cp ctor" << std::endl;
}
#endif

#if defined(MVCTOR)
Test::Test(Test &&other) : t{std::move(other.t)}
{
    other.print();
    print();
    std::cout << "mv ctor" << std::endl;
}
#endif

#if defined(CPASST)
Test &Test::operator=(const Test &other)
{
    other.print();
    print();
    t = other.t;
    std::cout << "cp asst" << std::endl;
    return *this;
}
#endif

#if defined(MVASST)
Test &Test::operator=(Test &&other)
{
    other.print();
    print();
    t = std::move(other.t);
    std::cout << "mv asst" << std::endl;
    return *this;
}
#endif

#if defined(DTOR)
Test::~Test()
{
    print();
    std::destroy_at<std::string>(&t);
    std::cout << "dtor" << std::endl;
}
#endif

void Test::print() const
{
    std::cout << "t = " << t << std::endl;
}
