#include <string>
#include <iostream>

class Test
{
public:
#if defined(CTOR)
    Test();
#endif

#if defined(CPCTOR)
    Test(const Test &);
#endif

#if defined(MVCTOR)
    Test(Test &&);
#endif

#if defined(CPASST)
    Test &operator=(const Test &);
#endif

#if defined(MVASST)
    Test &operator=(Test &&);
#endif

#if defined(DTOR)
    ~Test();
#endif

void print() const;

std::string t;
};
