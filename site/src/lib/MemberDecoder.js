import { items } from './Members'
import { inputToOutput } from './MemberData'

function decode(n) {
    return Array(items.length)
        .fill(false)
        .map((_, i) => !!(n & (1 << i)))
}

function encode(elements) {
    return Array.from(elements)
        .reduce((acc, cur, idx) => 
            cur ? acc | (1 << idx) : acc,
            0
        )
}

function computeMembers(elements) {
    // let a = encode(elements)
    // let b = inputToOutput[a]
    // console.log(a, b, decode(b))
    // return decode(b)
    return decode(inputToOutput[encode(elements)])
}

export {
    computeMembers
}