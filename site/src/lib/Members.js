const items = [
    {
        idx: 0,
        key: "CTOR",
        name: "Constructor",
        sample: "Animal();"
    },
    {
        idx: 1,
        key: "CPCTOR",
        name: "Copy constructor",
        sample: "Animal(const Animal &);"
    },
    {
        idx: 2,
        key: "MVCTOR",
        name: "Move constructor",
        sample: "Animal(Animal &&);"
    },
    {
        idx: 3,
        key: "CPASST",
        name: "Copy Assignment",
        sample: "Animal &operator=(const Animal &);"
    },
    {
        idx: 4,
        key: "MVASST",
        name: "Move Assignment",
        sample: "Animal &operator=(Animal &&);"
    },
    {
        idx: 5,
        key: "DTOR",
        name: "Destructor",
        sample: "~Animal();"
    }
];

export {
    items
}